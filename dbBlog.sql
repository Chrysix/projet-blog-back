USE appblog;

DROP TABLE user;
CREATE TABLE user(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (256),
    first_name VARCHAR (256),
    email VARCHAR (256) NOT NULL UNIQUE,
    password VARCHAR (256) NOT NULL
    
);

DROP TABLE topic;
CREATE TABLE topic (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR (256) NOT NULL,
    message VARCHAR (256) NOT NULL,
    picture VARCHAR (256),
    id_user INTEGER,
    FOREIGN KEY (id_user) REFERENCES user(id)
);

DROP TABLE topic_like;

CREATE TABLE topic_like (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    liked TINYINT (1) NOT NULL,
    id_user INTEGER NOT NULL,
    id_topic INTEGER NOT NULL,
    FOREIGN KEY (id_user) REFERENCES user(id),
    FOREIGN KEY (id_topic) REFERENCES topic(id)
)