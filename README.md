# Projet Blog

Je vous présente le projet que j'ai eu à faire pendant ma formation chez Simplon pour développer mes compétence en React et en Node/Express. 

Le but était de créer une plateforme de blog avec un back et un front. 

Elle permettra de pouvoir s'inscrire à celle-ci, si connecter et de poster des articles ainsi qu’un espace personel répertoriant les postes que l’on a envoyé.

# Projet : 

''

## Git du back:

https://gitlab.com/Chrysix/projet-blog-back


## Git du front:

https://gitlab.com/Chrysix/projet-blog-front

## User Storie

https://www.figma.com/file/V3oGe4MVM5xUZJPyMTmkv4/Projet-Blog---User-Case%2FStories


## Diagramme de classe

https://www.figma.com/file/eqOohMpbFWtNHtcimIHDtv/Projet-Blog---Diagramme-de-classes?node-id=0%3A1


## Maquette

https://www.figma.com/file/AD3wF5tPEvwNTcRzBu25J8/Projet-Blog---Maquette

