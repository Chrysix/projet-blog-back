import { Router } from "express";
import { TopicRepository } from "../repository/TopicRepository";
import passport from "passport";
export const topic_controller = Router();



//Ajout d'un Article
topic_controller.post("/", passport.authenticate('jwt', { session: false }), async(req, resp) => {
    try {
        await TopicRepository.add(req.body, req.user.id)
        resp.status(201).json(req.body)
    } catch (error) {
        console.log(error);
        resp.status(500).json(error)
    }

});

//Supression d'un Article
topic_controller.delete('/:id', async (req, resp) => {
    await TopicRepository.delete(req.params.id)
    resp.end()
})

 //Mise à jour d'un Article
topic_controller.patch('/:id', async (req, resp) => {
    try {
        let topic = await TopicRepository.findbyId(req.params.id);
        if (!topic) {
            resp.send(404).end();
            return
        }
        let update = {...topic, ...req.body };
        await TopicRepository.update(update);
        resp.json(update);
    } catch (error) {
        console.log(error);
        resp.status(400).end();
    }
});

//Montre un Article avec son id
topic_controller.get("/:id", async (req, resp)  =>{
    try {
        const topic = await TopicRepository.findbyId(req.params.id)
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})

//Montre tout les Articles
topic_controller.get("/", async (req, resp)  =>{
    try {
        const topic = await TopicRepository.findAll()
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})


topic_controller.get('/user/:id', passport.authenticate('jwt', { session: false }), async(req, resp)  =>{
    try {
        const topic = await TopicRepository.findbyUser(req.params.id)
        resp.status(201).json(topic)
        
    } catch (error) {
        console.log(error);
        resp.status(500).json(error);

    }
})