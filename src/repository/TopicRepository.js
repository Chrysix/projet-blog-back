import { Topic } from "../entity/Topic";
import { connection } from "./connection";

export class TopicRepository {

    //Ajout d'un Article
    static async add(topic, id_user){
        const [rows] = await connection.query('INSERT INTO topic (title, message, picture, id_user) VALUES (?, ?, ?, ?)', [topic.title, topic.message, topic.picture, id_user])
        topic.id = rows.insertId;
    };

    //Supresion d'un Article
    static async delete(id) {
        await connection.query("DELETE FROM topic WHERE id= ?", [id])
    }

    //Mise à jour d'un Article
    static async update(modif) {
        await connection.query('UPDATE topic SET title = ?, message = ?, picture = ? WHERE id = ?', [modif.title, modif.message, modif.picture, modif.id])

    }

    //Montre un Article avec son id
    static async findbyId(id) {
        const [rows] = await connection.query(`SELECT * FROM topic WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Topic(rows[0].title, rows[0].message, rows[0].picture);
            return instance
        }
        return null;
    }


    //eMontre un Article avec son id_user
    static async findbyUser(id) {
        const [rows] = await connection.query(`SELECT * FROM topic WHERE id_user=?`, [id]);
        return rows.map(row => new Topic(row['title'], row['message'], row['picture']));
    }

    //Montre tout les Articles
    static async findAll() {
        const [rows] = await connection.query(`SELECT * FROM topic`);
        return rows.map(row => new Topic(row['title'], row['message'], row['picture']));
        // for (const row of rows) {
        //     let instance = new Topic(row.title, row.message, rows.picture);
        //     article.push(instance);
        // }
        // return article
    }

}