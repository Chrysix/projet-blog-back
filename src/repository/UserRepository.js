import { User } from "../entity/User";
import { connection } from "./connection";

export class UserRepository {

    static async register(regist){
        const [rows] = await connection.query('INSERT INTO user (name, first_name, email, password) VALUES (?, ?, ?, ?)', [regist.name, regist.first_name, regist.email, regist.password])
        regist.id = rows.insertId;
    };

    static async findByEmail(email){
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email])
        if (rows.length === 1) {
            return new User(rows[0].name, rows[0].first_name, rows[0].email, rows[0].password, rows[0].id);
        };
        return null;
    };
}