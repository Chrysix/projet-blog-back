export class User {
    id;
    name;
    first_name;
    email;
    password;

    constructor(name, first_name, email, password, id){
        this.name = name , 
        this.first_name = first_name, 
        this.email = email, 
        this.password = password, 
        this.id = id
    }
}