export class Topic {
    id;
    title;
    message;
    picture;
    id_user;

    constructor(title, message, picture, id_user, id){
        this.title = title , 
        this.message = message,
        this.picture = picture,
        this.id_user = id_user,
        this.id = id
    }
}