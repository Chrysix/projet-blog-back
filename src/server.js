import express from 'express';
import cors from 'cors';
import { user_controller } from './controller/user_controller';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { topic_controller } from './controller/topic_controller';

configurePassport();

export const server = express();

server.use(passport.initialize());


server.use(express.json());
server.use(cors());

server.use('/api/user', user_controller)
server.use('/api/topic', topic_controller)