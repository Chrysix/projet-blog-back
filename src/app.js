import 'dotenv-flow/config';
import { server } from './server';
import { generateToken } from './utils/token';

const port = process.env.PORT || 8000;


server.listen(port, ()=> {
    console.log('listening on port '+port);
});


//Générateur de tokken quasi infini
// console.log(generateToken({
//     name: "machine",
//     first_name:'truc',
//     email: 'test@test.com',
//     id: 5,
    
// }, 9999));